import QtQuick 2.0
import Test 1.0

Rectangle {
    width: 360
    height: 360

    Sender{
        id: sender
    }

    ListModel{
        id: message
        ListElement{
            val: 1
            text : "userActive"
        }
        ListElement{
            val: 1
            text : "userInactive"
        }
        ListElement{
            val: 1
            text : "rightHand/swipeUp"
        }
        ListElement{
            val: 1
            text : "rightHand/swipeDown"
        }
    }

    ListView{
        anchors.fill: parent
        model: message
        delegate: Rectangle{
            width: ListView.view.width
            height: 50

            border.width: 1
            border.color: "cyan"

            Text{
                anchors.centerIn: parent
                text: model.text
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log('/1/' +  model.text, model.val)
                    sender.send('/1/' +  model.text, model.val)
                }
            }

        }
    }

}
