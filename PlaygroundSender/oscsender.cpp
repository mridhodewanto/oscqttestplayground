#include "oscsender.h"

OSCSender::OSCSender(QObject *parent) :
    QObject(parent)
{
    m_sender = new QOscClient( QHostAddress( QHostAddress::LocalHost ), 3333, 0 );

}

void OSCSender::send(const QString &path, const int &val)
{
    m_sender->sendData(path, val);
}
