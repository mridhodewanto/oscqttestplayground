import QtQuick 2.0
import Test 1.0

Rectangle {
    width: 360
    height: 360

    Receiver{
        id: r
        onData: {
            text.text = path + ' ' + val
        }
    }

    Text {
        id: text
        anchors.centerIn: parent
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }
}
