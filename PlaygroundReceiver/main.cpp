#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"

#include <QQuickItem>
#include "oscreceiver.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<OSCReceiver>("Test", 1, 0, "Receiver");

    QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/PlaygroundReceiver/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
